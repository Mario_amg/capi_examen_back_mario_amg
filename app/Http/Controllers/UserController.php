<?php
/**
 * Controller for examen_capi
 * PHP Version 7.3
 *
 * @category Controller
 * @package  examen_capi
 * @author   Mario May <marioamg.dev@gmail.com>
 * @license  proprietary software
 * @since    1.0.0
 */
namespace App\Http\Controllers;

use App\UserDomicilioModel;
use App\User;
use Illuminate\Http\Request;
use Lang;
use Config;
use Illuminate\Database\QueryException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserDomicilioModel  $userDomicilioModel
     * @return \Illuminate\Http\Response
     */
    public function show(UserDomicilioModel $userDomicilioModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserDomicilioModel  $userDomicilioModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserDomicilioModel $userDomicilioModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserDomicilioModel  $userDomicilioModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserDomicilioModel $userDomicilioModel)
    {
        //
    }

    public function domiciliosUsers()
    {
            $userAddress = User::all();
            for ($i=0; $i<count($userAddress); $i++) {
                $user = User::find($userAddress[$i]->id);
                $edades = $user->age;
                $domicilio = UserDomicilioModel::where('users_id', $user->id)
                ->get();
                $data[] = array('Usuario' => $user, 'Domicilio'=> $domicilio, 'Edad' => $edades . " años");
            }
            return ($data);
    }
}
