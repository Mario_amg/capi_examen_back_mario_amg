<?php

use Illuminate\Database\Seeder;

class UserDomicilioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\UserDomicilioModel::class, 100)->create();
    }
}
