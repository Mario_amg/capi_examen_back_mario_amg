<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UserDomicilioModel;
use Faker\Generator as Faker;

$factory->define(UserDomicilioModel::class, function (Faker $faker) {
    return [
        'users_id' => $faker->unique()->numberBetween(1, 100),
        'domicilio' => $faker->sentence(rand(5, 10)),
        'numero_exterior' => substr($faker->buildingNumber, 0, 5),
        'colonia' => $faker->sentence(5),
        'cp' => substr($faker->postcode, 0, 5),
        'ciudad' => $faker->sentence(7),
    ];
});
