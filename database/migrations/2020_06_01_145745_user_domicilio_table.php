<?php
/**
 * Migration for examen_capi
 * PHP Version 7.3
 *
 * @category Migration
 * @package  examen_capi
 * @author   Mario May <marioamg.dev@gmail.com>
 * @license  proprietary software
 * @since    1.0.0
 */
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserDomicilioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_domicilio_table', function (Blueprint $table) {
            $table->bigIncrements('user_id');
            $table->string('domicilio');
            $table->integer('numero_exterior');
            $table->string('colonia');
            $table->integer('cp');
            $table->string('ciudad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_domicilio_table');
    }
}
