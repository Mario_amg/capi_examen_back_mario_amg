<?php
/**
 * Model for examen_capi
 * PHP Version 7.3
 *
 * @category Model
 * @package  examen_capi
 * @author   Mario May <marioamg.dev@gmail.com>
 * @license  proprietary software
 * @since    1.0.0
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDomicilioModel extends Model
{
    /**
      * The attributes assignables
      *
      * @var array
      */
    protected $table = 'user_domicilio_table';
    protected $fillable = ['users_id', 'domicilio','numero_exterior','colonia','cp','ciudad'];
}
